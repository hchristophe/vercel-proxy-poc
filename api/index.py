from http.server import BaseHTTPRequestHandler
import requests
import json

# Disable SSL warnings
requests.packages.urllib3.disable_warnings()

# Lookup table for hostnames
with open('./lookup_table.json') as f:
  LOOKUP_TABLE = json.load(f)


class handler(BaseHTTPRequestHandler):

  def do_GET(self, body=True):
    # Parse request
    req_host = self.headers['host']
    print("Request host: {}".format(req_host))

    if req_host in LOOKUP_TABLE:
      hostname = LOOKUP_TABLE[self.headers['host']]
    else:
      hostname = LOOKUP_TABLE['default']
    # url = 'https://{}{}'.format(hostname, self.path)
    url = 'https://{}'.format(hostname)
    print("Target URL: {}".format(url))

    # Call the target service
    resp = requests.get(url, headers={}, verify=False)

    # Respond with the requested data
    self.send_response(resp.status_code)
    self.send_header('Content-type','text/html')
    self.end_headers()
    self.wfile.write(resp.content)

    return
      